import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Center(
        child: SizedBox(
        height: 200,
        child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: (){

            },
            child: Container(
              color: Colors.yellow,
              width: 200,
              height: 70,
              child: const Center(
                child: Text('Menu 1'),
              ),
            ),
          ),

          GestureDetector(
            onTap: (){

            },
            child: Container(
              color: Colors.amber,
              width: 200,
              height: 70,
              child: const Center(
                child: Text('Menu 2'),
              ),
            ),
          )
        ],
      ),
      ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          //navigator
        },
        child: const Text('+'),
      ),
    );
  }

}
